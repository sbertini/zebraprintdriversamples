package it.stefanobertini.zebra.samples;

import it.stefanobertini.zebra.NetworkPrintDriver;

import java.lang.reflect.Method;

public abstract class AbstractDriverSample {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public AbstractDriverSample(String[] args) {
	String address;
	int port;

	if (args.length < 1) {
	    System.out.println("Please, specify the printer's ip address");
	    System.exit(0);
	}
	address = args[0];
	port = args.length > 1 ? Integer.parseInt(args[1]) : 6101;

	NetworkPrintDriver driver = new NetworkPrintDriver(address, port);

	try {
	    driver.open();
	} catch (Exception e) {
	    e.printStackTrace();
	    System.exit(0);
	}

	Method[] methods;
	methods = this.getClass().getDeclaredMethods();

	for (int i = 0; i < methods.length; i++) {
	    Method method = methods[i];
	    if (method.isAnnotationPresent(Sample.class)) {

		Class[] params = method.getParameterTypes();

		if (params != null && params.length > 0
			&& params[0].isAssignableFrom(NetworkPrintDriver.class)) {
		    if (i > 0) {
			System.out.println("------------------");
		    }
		    System.out.println("Sample Method: " + method.getName());
		    System.out.println();
		    try {
			method.invoke(this, new Object[] { driver });
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		} else {
		    System.out.println("Method " + method.getName()
			    + " should have a NetworkPrintDriver argument.");
		}

	    }
	}
	driver.close();
    }

}
