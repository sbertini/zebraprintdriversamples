package it.stefanobertini.zebra.samples;

import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.PrintJob;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSample {

    public AbstractSample(String[] args) {
	String address;
	int port;
	List<SampleBean> jobs;

	address = args.length > 0 ? args[0] : null;
	port = args.length > 1 ? Integer.parseInt(args[1]) : 6101;

	// address = "192.168.1.133";
	// address = "127.0.0.1";

	jobs = detectSamples();

	for (int i = 0; i < jobs.size(); i++) {
	    SampleBean sample = jobs.get(i);
	    PrintJob<CommandInterface> job = sample.getPrintJob();

	    if (i > 0) {
		System.out.println("------------------");
	    }
	    System.out.println("Sample Method: " + sample.getMethod().getName());
	    System.out.println();
	    System.out.println("ToString():");
	    System.out.println(job.toString());
	    System.out.println();
	    System.out.println("Command Output:");
	    System.out.println(new String(job.getCommandByteArray()));

	    if (address != null) {
		NetworkPrintDriver driver = new NetworkPrintDriver(address, port);

		try {
		    driver.open();
		    driver.execute(job);
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    driver.close();
		}
	    }
	}
    }

    @SuppressWarnings("unchecked")
    private List<SampleBean> detectSamples() {
	ArrayList<SampleBean> detected = new ArrayList<SampleBean>();

	Method[] methods;
	methods = this.getClass().getDeclaredMethods();
	for (Method method : methods) {
	    if (method.isAnnotationPresent(Sample.class)) {
		PrintJob<CommandInterface> job;

		try {
		    job = (PrintJob<CommandInterface>) method.invoke(this, new Object[0]);
		} catch (Exception e) {
		    throw new RuntimeException(e);
		}

		SampleBean sample = new SampleBean(job, method);
		detected.add(sample);
	    }
	}
	return detected;
    }
}
