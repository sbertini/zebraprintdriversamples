package it.stefanobertini.zebra.samples;

import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.PrintJob;

import java.lang.reflect.Method;

public class SampleBean {

    private PrintJob<CommandInterface> printJob;
    private Method method;

    public SampleBean(PrintJob<CommandInterface> printJob, Method method) {
	super();
	this.printJob = printJob;
	this.method = method;
    }

    /**
     * @return the printJob
     */
    public PrintJob<CommandInterface> getPrintJob() {
	return printJob;
    }

    /**
     * @return the method
     */
    public Method getMethod() {
	return method;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "SampleBean [printJob=" + printJob + ", method=" + method + "]";
    }

}
