package it.stefanobertini.zebra.samples.cpcl.config;

import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.config.Do;
import it.stefanobertini.zebra.enums.ConfigurationKey;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class DoSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new DoSample(args);
    }

    public DoSample(String[] args) {
	super(args);
    }

    @Sample
    public void getSample(NetworkPrintDriver driver) throws Exception {

	CommandInterface command;
	command = new Do(ConfigurationKey.deviceReset);

	driver.executeNoWait(command);

    }
}
