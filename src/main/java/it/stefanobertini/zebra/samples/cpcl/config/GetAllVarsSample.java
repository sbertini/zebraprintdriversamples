package it.stefanobertini.zebra.samples.cpcl.config;

import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.config.GetVar;
import it.stefanobertini.zebra.enums.ConfigurationKey;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class GetAllVarsSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new GetAllVarsSample(args);
    }

    public GetAllVarsSample(String[] args) {
	super(args);
    }

    @Sample
    public void getSample(NetworkPrintDriver driver) throws Exception {

	CommandInterface command;
	StringBuffer buffer;
	String answer;

	long waitTimeout = 3000;
	for (ConfigurationKey key : ConfigurationKey.applicationDate
		.getDeclaringClass().getEnumConstants()) {

	    if (key.isGetVarAction()) {

		buffer = new StringBuffer();
		command = new GetVar(key);

		buffer.append(key);
		buffer.append(" (");
		buffer.append(key.getCode());
		buffer.append("): ");

		answer = driver.executeAndWait(command, waitTimeout);
		buffer.append(answer);

		System.out.println(buffer.toString());
	    }
	}

    }
}
