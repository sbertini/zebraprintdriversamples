package it.stefanobertini.zebra.samples.cpcl.config;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.config.GetVar;
import it.stefanobertini.zebra.cpcl.config.SetVar;
import it.stefanobertini.zebra.enums.ConfigurationKey;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class GetSetVarSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new GetSetVarSample(args);
    }

    public GetSetVarSample(String[] args) {
	super(args);
    }

    @Sample
    public void getSample(NetworkPrintDriver driver) throws Exception {

	String originalValue;
	String answer;

	driver.setDebugStream(System.out);
	driver.setDefaultAnswerWaitMillis(1000);

	originalValue = driver.executeAndWait(new GetVar(
		ConfigurationKey.bluetoothEnable));
	System.out.println("Original value: " + originalValue);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "off"));
	answer = driver.executeAndWait(new GetVar(
		ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(off): " + answer);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable, "on"));
	answer = driver.executeAndWait(new GetVar(
		ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(on): " + answer);

	driver.executeNoWait(new SetVar(ConfigurationKey.bluetoothEnable,
		originalValue));
	answer = driver.executeAndWait(new GetVar(
		ConfigurationKey.bluetoothEnable));
	System.out.println("After setVar(originalValue): " + answer);
    }
}
