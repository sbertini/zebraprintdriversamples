package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Barcode;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class BarcodeSample extends AbstractSample {

    public static void main(String[] args) {
	new BarcodeSample(args);
    }

    public BarcodeSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(150, 10), "HORIZ."));
	job.add(new Text(TextRotation.horizontal, new Font("7", 0), new Position(210, 60), "HORIZ."));

	job.add(new Barcode(Orientation.vertical, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(10, 200), "VERT."));
	job.add(new Text(TextRotation.vertical, new Font("7", 0), new Position(60, 140), "VERT."));

	job.add(new Print());

	return job;
    }
}
