package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Barcode;
import it.stefanobertini.zebra.cpcl.labelmode.BarcodeText;
import it.stefanobertini.zebra.cpcl.labelmode.BarcodeTextOff;
import it.stefanobertini.zebra.cpcl.labelmode.Journal;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class BarcodeTextSample extends AbstractSample {

    public static void main(String[] args) {
	new BarcodeTextSample(args);
    }

    public BarcodeTextSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 400, 1));

	job.add(new Journal());
	job.add(new Justification(JustificationType.center));

	job.add(new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(0, 20), "123456789", new Font("7", 0), 5));
	job.add(new Barcode(Orientation.vertical, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(4, 400), "123456789", new Font("7",
	        0), 5));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 400, 1));

	job.add(new Journal());
	job.add(new Justification(JustificationType.center));

	job.add(new BarcodeText(new Font("7", 0), 5));
	job.add(new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(0, 20), "123456789"));
	job.add(new Barcode(Orientation.vertical, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(4, 400), "123456789"));
	job.add(new BarcodeTextOff());
	job.add(new Print());

	return job;
    }
}
