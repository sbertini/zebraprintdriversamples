package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Comment;
import it.stefanobertini.zebra.cpcl.labelmode.Journal;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.cpcl.labelmode.Units;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.enums.UnitsType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class CommentSample extends AbstractSample {

    public static void main(String[] args) {
	new CommentSample(args);
    }

    public CommentSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 25, 1));
	job.add(new Units(UnitsType.millimeters));
	job.add(new Journal());
	job.add(new Comment("Center justify text"));
	job.add(new Justification(JustificationType.center));

	job.add(new Comment("Print the words ‘A COMMENT’"));

	job.add(new Text(TextRotation.horizontal, new Font("5", 1), new Position(0, 5), "A COMMENT"));

	job.add(new Print());

	return job;
    }
}
