package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Concat;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class ConcatSample extends AbstractSample {

    public static void main(String[] args) {
	new ConcatSample(args);
    }

    public ConcatSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	Concat concat = new Concat(Orientation.horizontal, new Position(75, 75));
	concat.addText(new Font("4", 2), 5, "$");
	concat.addText(new Font("4", 3), 0, "12");
	concat.addText(new Font("4", 2), 5, "34");
	job.add(concat);

	job.add(new Print());

	return job;
    }
}
