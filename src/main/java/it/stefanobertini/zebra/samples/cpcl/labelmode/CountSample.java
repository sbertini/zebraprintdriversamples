package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Barcode;
import it.stefanobertini.zebra.cpcl.labelmode.Count;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class CountSample extends AbstractSample {

    public static void main(String[] args) {
	new CountSample(args);
    }

    public CountSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 3));

	job.add(new Justification(JustificationType.center));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 50), "Testing 001"));
	job.add(new Count(1));

	job.add(new Text(TextRotation.horizontal, new Font("7", 0), new Position(0, 190), "Barcode Value is 123456789"));
	job.add(new Count(-10));

	job.add(new Barcode(Orientation.horizontal, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(0, 130), "123456789"));
	job.add(new Count(-10));

	job.add(new Print());

	return job;
    }

}
