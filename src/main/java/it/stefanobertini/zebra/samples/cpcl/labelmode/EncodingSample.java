package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Encoding;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.EncodingType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class EncodingSample extends AbstractSample {

    public static void main(String[] args) {
	new EncodingSample(args);
    }

    public EncodingSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 200, 1));

	job.add(new Encoding(EncodingType.gb18030));
	job.add(new Text(TextRotation.horizontal, new Font("GBUNSG24.CPF", 0), new Position(20, 30), "Font: GBUNSG24 ,t,u"));

	job.add(new Encoding(EncodingType.ascii));
	job.add(new Text(TextRotation.horizontal, new Font("7", 0), new Position(20, 80), "Font 7, size 0"));

	job.add(new Print());

	return job;
    }
}
