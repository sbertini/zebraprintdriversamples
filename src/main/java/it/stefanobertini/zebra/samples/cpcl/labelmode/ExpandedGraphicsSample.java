package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.Size;
import it.stefanobertini.zebra.cpcl.labelmode.End;
import it.stefanobertini.zebra.cpcl.labelmode.ExpandedGraphics;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class ExpandedGraphicsSample extends AbstractSample {

    public static void main(String[] args) {
	new ExpandedGraphicsSample(args);
    }

    public ExpandedGraphicsSample(String[] args) {
	super(args);
    }

    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new ExpandedGraphics(Orientation.horizontal, new Size(2, 16), new Position(90, 45),
	        "F0F0F0F0F0F0F0F00F0F0F0F0F0F0F0FF0F0F0F0F0F0F0F00F0F0F0F0F0F0F0F"));

	job.add(new End());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	int[] data = { 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0, 0xF0,
	        0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F };

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new ExpandedGraphics(Orientation.horizontal, new Size(2, 16), new Position(90, 45), data));

	job.add(new End());

	return job;
    }
}
