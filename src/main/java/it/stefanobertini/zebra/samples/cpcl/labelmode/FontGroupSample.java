package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Comment;
import it.stefanobertini.zebra.cpcl.labelmode.FontGroup;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class FontGroupSample extends AbstractSample {

    public static void main(String[] args) {
	new FontGroupSample(args);
    }

    public FontGroupSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 250, 1));
	job.add(new Comment("Specify fonts 0-0, 7-0, 5-0, 4-0 as members of font group 3."));
	job.add(new FontGroup(3).addFont(new Font("0", 0)).addFont(new Font("7", 0)).addFont(new Font("5", 0)).addFont(new Font("4", 0)));
	job.add(new Text(TextRotation.vertical, new Font("FG", 3), new Position(10, 250), "Ketchup"));
	job.add(new Text(TextRotation.vertical, new Font("FG", 3), new Position(70, 250), "Fancy Ketchup"));
	job.add(new Text(TextRotation.vertical, new Font("FG", 3), new Position(120, 250), "Extra Fancy Ketchup"));
	job.add(new Text(TextRotation.vertical, new Font("FG", 3), new Position(180, 250), "Large Size Extra Fancy Ketchup"));
	job.add(new Print());

	return job;
    }
}
