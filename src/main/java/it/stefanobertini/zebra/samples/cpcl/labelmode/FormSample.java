package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Form;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.cpcl.labelmode.Units;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.enums.UnitsType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class FormSample extends AbstractSample {

    public static void main(String[] args) {
	new FormSample(args);
    }

    public FormSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 3, 1));

	job.add(new Units(UnitsType.centimeters));
	job.add(new Justification(JustificationType.center));

	job.add(new Text(TextRotation.horizontal, new Font("4", 1), new Position(0, 0.5), "Form command"));

	job.add(new Form());
	job.add(new Print());

	return job;
    }
}
