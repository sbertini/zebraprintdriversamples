package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.InverseLine;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class InverseLineSample extends AbstractSample {

    public static void main(String[] args) {
	new InverseLineSample(args);
    }

    public InverseLineSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new Justification(JustificationType.center));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 45), "SAVE"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 95), "MORE"));

	job.add(new InverseLine(new Position(0, 45), new Position(145, 45), 45));
	job.add(new InverseLine(new Position(0, 95), new Position(145, 95), 45));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new Justification(JustificationType.center));

	job.add(new Text(TextRotation.horizontal, new Font("4", 3), new Position(30, 20), "$123.45"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 3), new Position(30, 70), "$678.90"));

	job.add(new InverseLine(new Position(25, 40), new Position(350, 40), 90));

	job.add(new Text(TextRotation.horizontal, new Font("4", 3), new Position(30, 120), "$432.10"));

	job.add(new Print());

	return job;
    }
}
