package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class JustificationSample extends AbstractSample {

    public static void main(String[] args) {
	new JustificationSample(args);
    }

    public JustificationSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 440, 1));

	job.add(new Justification(JustificationType.center));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 75), "C"));

	job.add(new Justification(JustificationType.left));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 75), "L"));

	job.add(new Justification(JustificationType.right));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 75), "R"));

	job.add(new Print());

	return job;
    }
}
