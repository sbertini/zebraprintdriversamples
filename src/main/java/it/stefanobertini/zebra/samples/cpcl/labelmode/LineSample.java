package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Line;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class LineSample extends AbstractSample {

    public static void main(String[] args) {
	new LineSample(args);
    }

    public LineSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new Line(new Position(0, 0), new Position(200, 0), 1));
	job.add(new Line(new Position(0, 0), new Position(200, 200), 2));
	job.add(new Line(new Position(0, 0), new Position(0, 200), 3));

	job.add(new Print());

	return job;
    }
}
