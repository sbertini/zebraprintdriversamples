package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Journal;
import it.stefanobertini.zebra.cpcl.labelmode.MaxiCode;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.MaxiCodeTag;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class MaxiCodeSample extends AbstractSample {

    public static void main(String[] args) {
	new MaxiCodeSample(args);
    }

    public MaxiCodeSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 600, 1));
	job.add(new Journal());

	MaxiCode maxiCode = new MaxiCode(new Position(20, 20));
	maxiCode.addTag(MaxiCodeTag.countryCode, "12345");
	maxiCode.addTag(MaxiCodeTag.msg, "This is a MAXICODE low priority message.");
	maxiCode.addTag(MaxiCodeTag.serviceClass, "12345");
	maxiCode.addTag(MaxiCodeTag.postalCode, "02886");
	job.add(maxiCode);
	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 600, 1));
	job.add(new Journal());

	MaxiCode maxiCode = new MaxiCode(new Position(20, 20));
	maxiCode.addTag(MaxiCodeTag.countryCode, "12345");
	maxiCode.addTag(MaxiCodeTag.msg, "This is a MAXICODE low priority message.");
	maxiCode.addTag(MaxiCodeTag.serviceClass, "12345");
	maxiCode.addTag(MaxiCodeTag.postalCode, "02886");
	maxiCode.addTag(MaxiCodeTag.zipper, "1");
	job.add(maxiCode);
	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample3() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 600, 1));
	job.add(new Journal());

	MaxiCode maxiCode = new MaxiCode(new Position(20, 20));
	maxiCode.addTag(MaxiCodeTag.addressValidation, "Y");
	maxiCode.addTag(MaxiCodeTag.shipToStreetAddress, "30 PLAN WAY");
	maxiCode.addTag(MaxiCodeTag.packageWeigh, "210");
	maxiCode.addTag(MaxiCodeTag.shipmentIdNumber, "42");
	maxiCode.addTag(MaxiCodeTag.julianPickupDay, "193");
	maxiCode.addTag(MaxiCodeTag.upsShipperNumber, "12345");
	maxiCode.addTag(MaxiCodeTag.trackingNumber, "1Z12345675");
	maxiCode.addTag(MaxiCodeTag.countryCode, "860");
	maxiCode.addTag(MaxiCodeTag.serviceClass, "1");
	maxiCode.addTag(MaxiCodeTag.postalCode, "02886");
	maxiCode.addTag(MaxiCodeTag.zipper, "1");
	maxiCode.addTag(MaxiCodeTag.upsShipperNumber, "12345E");
	maxiCode.addTag(MaxiCodeTag.packageNofX, "1/2");
	maxiCode.addTag(MaxiCodeTag.ups5, "1");
	maxiCode.addTag(MaxiCodeTag.shipToCity, "WARWICK");
	maxiCode.addTag(MaxiCodeTag.shipToState, "RI");
	job.add(maxiCode);
	job.add(new Print());

	return job;
    }
}
