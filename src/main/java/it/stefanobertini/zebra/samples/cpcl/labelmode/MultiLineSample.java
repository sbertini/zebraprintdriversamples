package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Multiline;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class MultiLineSample extends AbstractSample {

    public static void main(String[] args) {
	new MultiLineSample(args);
    }

    public MultiLineSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	Multiline multiLine = new Multiline(47, TextRotation.horizontal, new Font("4", 0), new Position(10, 20));
	multiLine.addText("1st line of text");
	multiLine.addText("2nd line of text");
	multiLine.addText(":");
	multiLine.addText("Nth line of text");
	job.add(multiLine);

	job.add(new Print());

	return job;
    }
}
