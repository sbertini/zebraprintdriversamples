package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Journal;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Pace;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class PaceSample extends AbstractSample {

    public static void main(String[] args) {
	new PaceSample(args);
    }

    public PaceSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 3));

	job.add(new Pace());

	job.add(new Journal());
	job.add(new Justification(JustificationType.center));

	job.add(new Text(TextRotation.horizontal, new Font("4", 1), new Position(0, 10), "Print 3 labels"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 1), new Position(0, 90), "Using PACE"));

	job.add(new Print());

	return job;
    }
}
