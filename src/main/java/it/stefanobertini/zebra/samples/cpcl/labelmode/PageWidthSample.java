package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.PageWidth;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class PageWidthSample extends AbstractSample {

    public static void main(String[] args) {
	new PageWidthSample(args);
    }

    public PageWidthSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	Font font = new Font("4", 0);

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	job.add(new PageWidth(150));

	job.add(new Justification(JustificationType.left));

	job.add(new Text(TextRotation.horizontal, font, new Position(0, 0), "TEXT"));
	job.add(new Print());

	return job;
    }
}
