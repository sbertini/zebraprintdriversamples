package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Comment;
import it.stefanobertini.zebra.cpcl.labelmode.Line;
import it.stefanobertini.zebra.cpcl.labelmode.Pattern;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.ScaleText;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.PatternType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class PatternSample extends AbstractSample {

    public static void main(String[] args) {
	new PatternSample(args);
    }

    public PatternSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 700, 1));

	job.add(new Comment("Draw horizontal and vertical patterns"));
	job.add(new Pattern(PatternType.horizontalLines));
	job.add(new Line(new Position(10, 10), new Position(160, 10), 42));
	job.add(new Pattern(PatternType.verticalLines));
	job.add(new Line(new Position(170, 10), new Position(350, 10), 42));

	job.add(new Comment("Draw left and right diagonal patterns"));
	job.add(new Pattern(PatternType.rightRisingDiagonalLines));
	job.add(new Line(new Position(10, 65), new Position(160, 65), 42));
	job.add(new Pattern(PatternType.leftRisingDiagonalLines));
	job.add(new Line(new Position(170, 65), new Position(350, 65), 42));

	job.add(new Comment("Draw square and cross hatch patterns"));
	job.add(new Pattern(PatternType.squarePattern));
	job.add(new Line(new Position(10, 115), new Position(160, 115), 42));
	job.add(new Pattern(PatternType.crossHatckPattern));
	job.add(new Line(new Position(170, 115), new Position(350, 115), 42));

	job.add(new Comment("Draw a scalable text character with cross hatch pattern"));
	job.add(new Pattern(PatternType.crossHatckPattern));
	job.add(new ScaleText(Orientation.horizontal, "PLL_LAT.CSF", 40, 40, new Position(20, 180), "HELLO"));

	job.add(new Print());

	return job;
    }

}
