package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Pcx;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

import java.io.File;

public class PcxSample extends AbstractSample {

    public static void main(String[] args) {
	new PcxSample(args);
    }

    public PcxSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 0), "Pcx from resource"));
	job.add(new Pcx(new Position(0, 50)).loadImageFromResource("zebra.pcx"));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 0), "Pcx from file"));
	job.add(new Pcx(new Position(0, 50)).loadImageFromFile(new File("./target/classes/zebra.pcx")));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample3() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 0), "Pcx from printer"));
	job.add(new Pcx(new Position(0, 50)).loadImageFromPrinter("zebra.pcx"));

	job.add(new Print());

	return job;
    }
}
