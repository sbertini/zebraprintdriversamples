package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Pdf417;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.ArrayList;
import java.util.List;

public class Pdf417Sample extends AbstractSample {

    public static void main(String[] args) {
	new Pdf417Sample(args);
    }

    public Pdf417Sample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));

	List<String> data = new ArrayList<String>();
	data.add("PDF Data");
	data.add("ABCDE12345");
	job.add(new Pdf417(Orientation.horizontal, new Position(10, 20), 3, 12, 3, 2, data));

	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(10, 120), "PDF Data"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(10, 170), "ABCDE12345"));

	job.add(new Print());

	return job;
    }
}
