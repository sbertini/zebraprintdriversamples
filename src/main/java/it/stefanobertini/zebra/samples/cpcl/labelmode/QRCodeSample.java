package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.QRCodeAutomaticData;
import it.stefanobertini.zebra.beans.QRCodeDataInterface;
import it.stefanobertini.zebra.beans.QRCodeManualData;
import it.stefanobertini.zebra.beans.QRCodeManualDataItem;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.QRCode;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.QRCodeCharacterMode;
import it.stefanobertini.zebra.enums.QRCodeErrorCorrectionLevel;
import it.stefanobertini.zebra.enums.QRCodeMaskNumber;
import it.stefanobertini.zebra.enums.QRCodeModelType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class QRCodeSample extends AbstractSample {

    public static void main(String[] args) {
	new QRCodeSample(args);
    }

    public QRCodeSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 500, 1));

	QRCodeDataInterface data;
	data = new QRCodeAutomaticData(QRCodeErrorCorrectionLevel.standard, QRCodeMaskNumber.none, "QR Code");

	QRCode qrCode = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	job.add(qrCode);
	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 500, 1));

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.ultraHighReliability, QRCodeMaskNumber.mask0);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.numeric, "0123456789012345"));

	QRCode qrCode = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	job.add(qrCode);
	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample3() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 500, 1));

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.ultraHighReliability, QRCodeMaskNumber.mask0);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "AC-42"));

	QRCode qrCode = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	job.add(qrCode);
	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample4() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 500, 1));

	QRCodeManualData data;
	data = new QRCodeManualData(QRCodeErrorCorrectionLevel.highDensity, QRCodeMaskNumber.none);
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "QR code"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.numeric, "0123456789012345"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.alphanumeric, "QRCODE"));
	data.addData(new QRCodeManualDataItem(QRCodeCharacterMode.binary, "qrcode"));

	QRCode qrCode = new QRCode(Orientation.horizontal, new Position(10, 100), QRCodeModelType.model2, 10, data);

	job.add(qrCode);
	job.add(new Print());

	return job;
    }
}
