package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.Rotate;
import it.stefanobertini.zebra.cpcl.labelmode.ScalableConcat;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class RotateSample extends AbstractSample {

    public static void main(String[] args) {
	new RotateSample(args);
    }

    public RotateSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 440, 1));

	job.add(new Justification(JustificationType.center));

	job.add(new Text(TextRotation.horizontal, new Font("4", 1), new Position(0, 50), "Rotate Strings"));

	job.add(new Rotate(45));

	ScalableConcat scalableConcat = new ScalableConcat(Orientation.horizontal, new Position(50, 300));
	scalableConcat.addScalableText("PLL_LAT.CSF", 20, 20, 20, "$");
	scalableConcat.addScalableText("PLL_LAT.CSF", 40, 40, 0, "22");
	scalableConcat.addScalableText("PLL_LAT.CSF", 20, 20, 0, "99");
	job.add(scalableConcat);

	job.add(new Print());

	return job;
    }
}
