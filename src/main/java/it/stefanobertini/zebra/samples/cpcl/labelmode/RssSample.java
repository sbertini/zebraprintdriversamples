package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.Rss;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.RssBarcodeType;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class RssSample extends AbstractSample {

    public static void main(String[] args) {
	new RssSample(args);
    }

    public RssSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 40), "RSS14 Composite"));
	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 70), "1234567890123|1234567890"));

	job.add(new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14, "1234567890123", "1234567890"));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 40), "RSS14S (type='3')"));
	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 70), "1234567890123"));

	job.add(new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14_Stacked, "1234567890123", ""));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample3() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 40), "RSSEXP (type='6')"));
	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 70), "1234567890123"));

	job.add(new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_Expanded, "1234567890123", ""));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample4() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 400, 1));

	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 40), "UCC128A (type='11')"));
	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 70), "12345678901234567890|1234567890"));

	job.add(new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.UCC_128_Composite_A_B, "12345678901234567890", "1234567890"));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample5() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 400, 1));

	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 40), "RSS14 (type='1')"));
	job.add(new Text(TextRotation.horizontal, new Font("5", 0), new Position(10, 70), "1011234567890"));

	job.add(new Rss(Orientation.horizontal, new Position(10, 110), 2, 25, 3, 22, RssBarcodeType.RSS_14, "1011234567890", ""));

	job.add(new Print());

	return job;
    }
}
