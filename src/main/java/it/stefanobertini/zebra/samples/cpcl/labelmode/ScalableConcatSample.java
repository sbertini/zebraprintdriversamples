package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.ScalableConcat;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class ScalableConcatSample extends AbstractSample {

    public static void main(String[] args) {
	new ScalableConcatSample(args);
    }

    public ScalableConcatSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Justification(JustificationType.center));

	ScalableConcat scalableConcat = new ScalableConcat(Orientation.horizontal, new Position(0, 20));
	scalableConcat.addBitmapText(new Font("4", 1), 0, "2/");
	scalableConcat.addScalableText("PLL_LAT.CSF", 20, 20, 15, "$");
	scalableConcat.addScalableText("PLL_LAT.CSF", 40, 40, 0, "22");
	scalableConcat.addScalableText("PLL_LAT.CSF", 20, 20, 0, "99");
	job.add(scalableConcat);

	job.add(new Print());

	return job;
    }

}
