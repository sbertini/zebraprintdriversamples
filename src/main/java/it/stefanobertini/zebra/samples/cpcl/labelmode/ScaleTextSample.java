package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.ScaleText;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class ScaleTextSample extends AbstractSample {

    public static void main(String[] args) {
	new ScaleTextSample(args);
    }

    public ScaleTextSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Justification(JustificationType.center));
	job.add(new ScaleText(Orientation.horizontal, "PLL_LAT.CSF", 10, 10, new Position(0, 10), "10 POINT FONT"));
	job.add(new ScaleText(Orientation.horizontal, "PLL_LAT.CSF", 20, 10, new Position(0, 80), "WIDER FONT"));
	job.add(new ScaleText(Orientation.horizontal, "PLL_LAT.CSF", 10, 20, new Position(0, 150), "TALLER FONT"));

	job.add(new Print());

	return job;
    }

}
