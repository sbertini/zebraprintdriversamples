package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.ScaleToFit;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Units;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.UnitsType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class ScaleToFitSample extends AbstractSample {

    public static void main(String[] args) {
	new ScaleToFitSample(args);
    }

    public ScaleToFitSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 300, 1));

	job.add(new Justification(JustificationType.center));
	job.add(new Units(UnitsType.millimeters));
	job.add(new ScaleToFit(Orientation.horizontal, "PLL_LAT.CSF", 40, 10, new Position(0, 10), "SALE"));
	job.add(new ScaleToFit(Orientation.horizontal, "PLL_LAT.CSF", 40, 10, new Position(0, 20), "SALE PRICE"));
	job.add(new ScaleToFit(Orientation.horizontal, "PLL_LAT.CSF", 40, 20, new Position(0, 30), "SALE"));

	job.add(new Print());

	return job;
    }

}
