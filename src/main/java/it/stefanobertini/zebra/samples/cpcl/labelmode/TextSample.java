package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class TextSample extends AbstractSample {

    public static void main(String[] args) {
	new TextSample(args);
    }

    public TextSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample() {

	Font font = new Font("4", 0);

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 210, 1));
	job.add(new Text(TextRotation.horizontal, font, new Position(200, 100), "TEXT"));
	job.add(new Text(TextRotation.rotate90, font, new Position(200, 100), "T90"));
	job.add(new Text(TextRotation.rotate180, font, new Position(200, 100), "T180"));
	job.add(new Text(TextRotation.rotate270, font, new Position(200, 100), "T270"));
	job.add(new Print());

	return job;
    }
}
