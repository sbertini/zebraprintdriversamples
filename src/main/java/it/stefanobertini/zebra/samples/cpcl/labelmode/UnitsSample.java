package it.stefanobertini.zebra.samples.cpcl.labelmode;

import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Barcode;
import it.stefanobertini.zebra.cpcl.labelmode.Print;
import it.stefanobertini.zebra.cpcl.labelmode.StartPrint;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.cpcl.labelmode.Units;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.TextRotation;
import it.stefanobertini.zebra.enums.UnitsType;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class UnitsSample extends AbstractSample {

    public static void main(String[] args) {
	new UnitsSample(args);
    }

    public UnitsSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample1() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0.3937, 1, 1));

	job.add(new Units(UnitsType.inches));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 0), "1 cm = 0.3937”"));

	job.add(new Units(UnitsType.dots));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 48), "1 mm = 8 dots"));

	job.add(new Barcode(Orientation.horizontal, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 48, new Position(16, 112), "UNITS"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(40, 160), "UNITS"));

	job.add(new Print());

	return job;
    }

    @Sample
    public PrintJob<LabelModeCommandInterface> getSample2() {

	PrintJob<LabelModeCommandInterface> job = new PrintJob<LabelModeCommandInterface>();
	job.add(new StartPrint(0, 2.54, 1));

	job.add(new Units(UnitsType.centimeters));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(1, 0), "1” = 2.54 cm"));

	job.add(new Units(UnitsType.millimeters));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(0, 6), "203 dots = 25.4 mm"));

	job.add(new Barcode(Orientation.horizontal, BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 0.125, 16, new Position(12, 14), "UNITS"));
	job.add(new Text(TextRotation.horizontal, new Font("4", 0), new Position(16, 20), "UNITS"));

	job.add(new Print());

	return job;
    }
}
