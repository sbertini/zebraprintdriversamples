package it.stefanobertini.zebra.samples.cpcl.linemode;

import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.linemode.BeginPage;
import it.stefanobertini.zebra.cpcl.linemode.EndPage;
import it.stefanobertini.zebra.cpcl.linemode.Move;
import it.stefanobertini.zebra.cpcl.linemode.SetLinePrintFont;
import it.stefanobertini.zebra.cpcl.linemode.Text;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

public class MoveSample extends AbstractSample {

    public static void main(String[] args) {
        new MoveSample(args);
    }

    public MoveSample(String[] args) {
        super(args);
    }

    @Sample
    public PrintJob<LineModeCommandInterface> getSampleAbsolute() {

        PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
        job.add(new BeginPage());
        job.add(new SetLinePrintFont(new Font("4", 0), 47));

        job.add(new Text("Absolute"));

        int i = 1;

        for (int j = 0; j < 5; j++) {
            job.add(new Move(new Position(50 * j, 50)));
            job.add(new Text("" + i++, false));
        }

        for (int j = 0; j < 5; j++) {
            job.add(new Move(new Position(50 * j, 100)));
            job.add(new Text("" + i++, false));
        }

        job.add(new Text(""));
        job.add(new EndPage());

        return job;
    }

    @Sample
    public PrintJob<LineModeCommandInterface> getSampleRelative() {

        PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
        job.add(new BeginPage());
        job.add(new SetLinePrintFont(new Font("4", 0), 47));

        job.add(new Text("Relative"));

        int i = 1;

        for (int z = 1; z < 3; z++) {

            job.add(new Move(50, Orientation.vertical, true));

            for (int j = 0; j < 5; j++) {
                job.add(new Text("" + i++, false));
                job.add(new Move(50, Orientation.horizontal, true));
            }

            job.add(new Move(0, Orientation.horizontal, false));
        }

        job.add(new Text(""));
        job.add(new EndPage());

        return job;
    }
}
