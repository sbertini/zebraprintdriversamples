package it.stefanobertini.zebra.samples.cpcl.linemode;

import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.cpcl.config.GetVar;
import it.stefanobertini.zebra.cpcl.config.SetVar;
import it.stefanobertini.zebra.cpcl.linemode.Barcode;
import it.stefanobertini.zebra.cpcl.linemode.BeginPage;
import it.stefanobertini.zebra.cpcl.linemode.Cut;
import it.stefanobertini.zebra.cpcl.linemode.EndPage;
import it.stefanobertini.zebra.cpcl.linemode.Journal;
import it.stefanobertini.zebra.cpcl.linemode.Justification;
import it.stefanobertini.zebra.cpcl.linemode.PartialCut;
import it.stefanobertini.zebra.cpcl.linemode.SetLinePrintFont;
import it.stefanobertini.zebra.cpcl.linemode.SetSpacing;
import it.stefanobertini.zebra.cpcl.linemode.Text;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.ConfigurationKey;
import it.stefanobertini.zebra.enums.JustificationType;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.ArrayList;
import java.util.List;

public class ReceiptSample extends AbstractDriverSample {

    public static void main(String[] args) {
        new ReceiptSample(args);
    }

    public ReceiptSample(String[] args) {
        super(args);
    }

    @Sample
    public void getSample1(NetworkPrintDriver driver) throws Exception {

        String originalValue;
        String answer;

        driver.setDebugStream(System.out);
        driver.setDefaultAnswerWaitMillis(1000);

        // Enable line mode (default should be hybrid_xml_zpl)
        originalValue = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Original value: " + originalValue);

        driver.executeNoWait(new SetVar(ConfigurationKey.deviceLanguages, "line_print"));

        answer = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Current value: " + answer);

        PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
        job.add(new BeginPage());
        job.add(new Journal());

        job.add(new SetLinePrintFont(new Font("4", 0), 47));
        job.add(new Text("YOURCO RETAIL STORES"));
        job.add(new Text(""));

        job.add(new SetLinePrintFont(new Font("7", 0), 24));

        List<String> data = new ArrayList<String>();
        data.add("14:40 PM Thursday, 06/04/20");
        data.add("Quantity  Item          Unit     Total");
        data.add("1         Babelfish     $4.20    $4.20");
        data.add("          Tax:          5%       $0.21");
        job.add(new Text(data));
        job.add(new Text(""));

        job.add(new SetSpacing(5));
        job.add(new Text("Total:", false));

        job.add(new SetSpacing(0));
        job.add(new Text("$4.41", true));

        job.add(new Text("Thank you for shopping at YOURCO"));

        job.add(new PartialCut());

        job.add(new EndPage());

        driver.execute(job);

        // Reset original print mode
        driver.executeAndWait(new SetVar(ConfigurationKey.deviceLanguages, originalValue));

        answer = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Current value: " + answer);

    }

    @Sample
    public void getSample2(NetworkPrintDriver driver) throws Exception {

        String originalValue;
        String answer;

        driver.setDebugStream(System.out);
        driver.setDefaultAnswerWaitMillis(1000);

        // Enable line mode (default should be hybrid_xml_zpl)
        originalValue = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Original value: " + originalValue);

        driver.executeAndWait(new SetVar(ConfigurationKey.deviceLanguages, "line_print"));

        answer = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Current value: " + answer);

        PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
        job.add(new BeginPage());
        job.add(new Journal());

        job.add(new SetLinePrintFont(new Font("5", 2), 46));
        job.add(new Text("AURORA'S FABRIC SHOP"));
        job.add(new Text(""));

        job.add(new SetLinePrintFont(new Font("5", 0), 24));

        List<String> data = new ArrayList<String>();
        data.add("       123 Castle Drive, Kingston, RI 02881");
        data.add("                 (401) 555-4CUT");
        job.add(new Text(data));

        job.add(new Justification(JustificationType.left));

        job.add(new SetLinePrintFont(new Font("7", 0), 24));
        data = new ArrayList<String>();
        data.add("");
        data.add("4:20 PM Thursday, June 04, 2020 Store: 142 ");
        data.add("Order Number: #59285691");
        job.add(new Text(data));

        job.add(new Text("Status: ", false));

        job.add(new SetSpacing(10));
        job.add(new Text("Incomplete", true));
        job.add(new SetSpacing(0));

        data = new ArrayList<String>();
        data.add("");
        data.add("Item Description      Quant.   Price  Subtot.Tax");
        data.add("");
        data.add("1211 45\" Buckram    5 yds@  $3.42/yd  $17.10  Y");
        data.add("Z121 60\" Blue Silk 10 yds@ $15.00/yd $150.00  N");
        data.add("Z829 60\" Muslin    20 yds@  $1.00/yd  $20.00  Y");
        data.add("");
        data.add("          SUBTOTAL:                      $187.10");
        data.add("RHODE ISLAND SALES TAX 7.00%:              $2.60");
        data.add("TOTAL:                                   $189.70");
        data.add("");
        job.add(new Text(data));

        job.add(new SetLinePrintFont(new Font("7", 1), 48));
        data = new ArrayList<String>();
        data.add("   PLEASE BRING THIS RECEIPT TO THE CASHIER");
        data.add("        WITH THE REST OF YOUR PURCHASES.");
        data.add("");
        job.add(new Text(data));

        job.add(new Justification(JustificationType.center));
        job.add(new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 2, 100, "59285691 ST 187.10 T 2.60"));

        job.add(new Cut());

        job.add(new EndPage());

        driver.execute(job);

        // Reset original print mode
        driver.executeAndWait(new SetVar(ConfigurationKey.deviceLanguages, originalValue));

        answer = driver.executeAndWait(new GetVar(ConfigurationKey.deviceLanguages));
        System.out.println("Current value: " + answer);

    }
}
