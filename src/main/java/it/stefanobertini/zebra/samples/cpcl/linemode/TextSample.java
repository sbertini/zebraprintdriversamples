package it.stefanobertini.zebra.samples.cpcl.linemode;

import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.cpcl.linemode.BeginPage;
import it.stefanobertini.zebra.cpcl.linemode.EndPage;
import it.stefanobertini.zebra.cpcl.linemode.Text;
import it.stefanobertini.zebra.samples.AbstractSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.ArrayList;
import java.util.List;

public class TextSample extends AbstractSample {

    public static void main(String[] args) {
	new TextSample(args);
    }

    public TextSample(String[] args) {
	super(args);
    }

    @Sample
    public PrintJob<LineModeCommandInterface> getSample() {

	PrintJob<LineModeCommandInterface> job = new PrintJob<LineModeCommandInterface>();
	job.add(new BeginPage());

	job.add(new Text("This is on a line"));
	job.add(new Text(""));

	job.add(new Text("Also this is on ", false));
	job.add(new Text("a line"));
	job.add(new Text(""));

	job.add(new Text("This is on\r\ntwo lines"));
	job.add(new Text(""));

	List<String> data = new ArrayList<String>();

	data.add("Line 1");
	data.add("Line 2");
	data.add(":");
	data.add("Line n");

	job.add(new Text(data));
	job.add(new Text(""));

	data = new ArrayList<String>();

	data.add("This ");
	data.add(" is");
	data.add(" on");
	data.add(" a");
	data.add(" line");
	data.add("\r\n");
	job.add(new Text(data, false));

	job.add(new EndPage());

	return job;
    }
}
