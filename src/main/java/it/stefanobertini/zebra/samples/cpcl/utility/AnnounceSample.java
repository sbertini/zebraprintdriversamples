package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.Announce;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class AnnounceSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new AnnounceSample(args);
    }

    public AnnounceSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new Announce("3 2 1 0"));
	System.out.println("Answer:\r\n" + answer);

    }

}
