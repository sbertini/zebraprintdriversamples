package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.GetDate;
import it.stefanobertini.zebra.cpcl.utility.SetDate;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.Date;

public class DateSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new DateSample(args);
    }

    public DateSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new GetDate());
	System.out.println("Answer:" + answer);

	answer = driver.executeAndWait(new SetDate(new Date()));
	System.out.println("Answer:" + answer);

	answer = driver.executeAndWait(new GetDate());
	System.out.println("Answer:" + answer);
    }

}
