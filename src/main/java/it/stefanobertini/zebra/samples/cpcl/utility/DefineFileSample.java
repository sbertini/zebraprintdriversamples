package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.DefineFile;
import it.stefanobertini.zebra.enums.DefineFileTerminator;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.ArrayList;
import java.util.List;

public class DefineFileSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new DefineFileSample(args);
    }

    public DefineFileSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;
	String filename = "test.txt";
	List<String> data = new ArrayList<String>();

	driver.setDebugStream(System.out);

	data.add("Line1");
	data.add("Line2");
	data.add("Line3");

	answer = driver.executeAndWait(new DefineFile(filename, data, DefineFileTerminator.end));

	System.out.println("Answer: " + answer);

    }

}
