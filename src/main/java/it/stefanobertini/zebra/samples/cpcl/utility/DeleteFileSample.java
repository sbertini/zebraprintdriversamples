package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.DeleteFile;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class DeleteFileSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new DeleteFileSample(args);
    }

    public DeleteFileSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new DeleteFile("TEST.TXT"));
	System.out.println("Answer:\r\n" + answer);

    }

}
