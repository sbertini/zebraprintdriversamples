package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.Dir;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class DirSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new DirSample(args);
    }

    public DirSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new Dir());
	System.out.println("Answer:\r\n" + answer);

    }

}
