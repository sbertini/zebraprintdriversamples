package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.GetTime;
import it.stefanobertini.zebra.cpcl.utility.SetTime;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

import java.util.Date;

public class TimeSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new TimeSample(args);
    }

    public TimeSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new GetTime());
	System.out.println("Answer:" + answer);

	answer = driver.executeAndWait(new SetTime(new Date()));
	System.out.println("Answer:" + answer);

	answer = driver.executeAndWait(new GetTime());
	System.out.println("Answer:" + answer);
    }

}
