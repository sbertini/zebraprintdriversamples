package it.stefanobertini.zebra.samples.cpcl.utility;

import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.cpcl.utility.Version;
import it.stefanobertini.zebra.samples.AbstractDriverSample;
import it.stefanobertini.zebra.samples.Sample;

public class VersionSample extends AbstractDriverSample {

    public static void main(String[] args) {
	new VersionSample(args);
    }

    public VersionSample(String[] args) {
	super(args);
    }

    @Sample
    public void execute(NetworkPrintDriver driver) throws Exception {

	String answer;

	driver.setDebugStream(System.out);

	answer = driver.executeAndWait(new Version());
	System.out.println("Answer: " + answer);

    }

}
